<?php
echo <<<HTML
<!DOCTYPE html>
<title>Cirurgia de Catarata - Instituto Catarata</title>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/style.min.css" />
    <link href="assets/css/fontawesome/all.css" rel="stylesheet" />
    <link rel="shortcut icon" href ="images/icon.png"> 
</head>

<body>
    <section class="topnav">
        <div>
            <span>Ligue e Agende Sua ligação: <b>(11) 2942-7778</b></span>
        </div>
    </section>
    <section class="contentLogo">
        <img src="images/catarata.png">
    </section>
    <section class="grad"></section>

    <section class="contentBanner">
        <div class="right">
            <p><strong>Volte a ver o mundo com transparência</strong></p>
            <p><strong>CIRURGIA DE CATARATA</strong></p>
            <p><strong>Parcelamento em 12x sem Juros</strong></p>
            <p><strong>Pacote Completo com Lente Importada e Exames</strong></p>
            <input type="button" value="RECEBER PREÇO DA CIRURGIA">
        </div>
    </section>

    <section class="contentHistory">
        <header>
            <h1>NOSSA HISTÓRIA</h1>
        </header>
        <div class="row">
            <div class="column">
                <img src="images/catarata2.jpg">
            </div>
            <div class="column">
                <h2>Cirugia de Catarata Acessivel</h2>
                <p>somos médicos oftalmologistas espercialistas em cirurgia de recuperação da visão.</p>
                <p>Fundamos o Instituto a fim de ajudar as pessoas a realizarem a cirurgia de catarata de forma rápida e com toda a segurança que uma equipe médica experiente pode proporcionar.</p>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="column"><i class="fas fa-user-md fa-6x"></i><p>Equipe Cirúrgica Experiente</p></div>
        <div class="column"><i class="far fa-heart fa-6x"></i><p>Financiamento Próprio</p></div>
        <div class="column"><i class="fas fa-credit-card  fa-6x"></i><p>Cirurgia em 12x sem Juros</p></div>
    </section>

    <section class="Banner">
        <div class="cover"> 
            <h1>A CIRURGIA DE CATARATA</h1>
            <p>já restarou a visão de milhões de pessoas</p>
        </div>
    </section>

    <section class="filiadas">
        <header><span><strong>CLÍNICAS AFILIDAS</strong></span></header>
        <div class="row">
        <div class="column">
            <img src="images/filiadas.png ">
            <footer>
                <h4>UNIDADE TATUAPÉ</h4>
                <p><strong>Clínica Olhar Certo</strong></p>
                <p>Rua Tuiuti,2.429</p>
                <p>São Paulo-SP</p>
                <p>(11)2942-7778</p>
            </footer>
        </div>
        <div class="column"> 
            <img src="images/filiadas2.png">
            <footer>
                <h4>UNIDADE JARDIM IGUATEMI</h4>
                <p><strong>Clínica Olhar Certo</strong></p>
                <p>Av. Ragueb Chohfi, 4.440</p>
                <p>São Paulo-SP</p>
                <p>(11)2942-7778</p>
            </footer>
        </div>
        </div>
        <div id="ofertas"><input type="button" value="RECEBER PREÇO DE CIRURGIA"></div>
    </section>
    
    <section class="duvidas">
        <span>DÚVIDAS?</span>
        <input type="button" value="CONVERSE VIA WHATSAPP">
    </section>
    <footer class="footer">
        <p>Instituto Catarata de São Paulo</p>
        <p>Responsável Técnico - Dra. Priscila Rodrigues de Almeida - CRM-SP 148173</p>
        <p>CNPJ:21.182.295/0001-34</p>
        <p>Política de Privacidade</p>
    </footer>

</body>

</html>
HTML;
?>